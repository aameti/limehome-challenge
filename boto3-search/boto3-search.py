import boto3
import botocore

def find_files_with_substring(s3_bucket_name, substring):
    s3 = boto3.client('s3')

    file_names = []
    try:
        # List objects within the bucket
        bucket_contents = s3.list_objects(Bucket=s3_bucket_name)['Contents']
        for file in bucket_contents:
            file_name = file['Key']
            if file_name.endswith('.txt'):
                file_obj = s3.get_object(Bucket=s3_bucket_name, Key=file_name)
                file_content = file_obj['Body'].read().decode('utf-8')
                if substring in file_content:
                    file_names.append(file_name)
    except botocore.exceptions.ClientError as e:
        print(f"An error occurred: {e}")

    return file_names

# Example usage
s3_bucket_name = 'your-bucket-name'
substring = 'search-term'
print(find_files_with_substring(s3_bucket_name, substring)) 
