
# S3 Text File Searcher

This Python script is designed to iterate through an Amazon Web Services (AWS) Simple Storage Service (S3) bucket to find and list all text files that contain a specified substring.

## Features

- **Bucket Iteration**: Iterates through each file in the specified S3 bucket.
- **Substring Searching**: Searches for a user-defined substring within the text files.
- **File Filtering**: Processes only `.txt` files (modifiable for other file types).

## Requirements

- Python 3
- Boto3 library
- AWS Credentials (with S3 read access)

## Setup

### Install Dependencies

Python 3 is required. Install Boto3, the AWS SDK for Python, using pip:

```bash
pip install boto3
```

### Configure AWS Credentials

Configure your AWS credentials to allow script access to your AWS account. This can be done via:

1. **AWS CLI**: Configure using the AWS Command Line Interface (`aws configure`).
2. **Environment Variables**: Set `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` in your environment.

Ensure the AWS user has the necessary permissions for the specified S3 bucket.

## Usage

### Basic Usage

1. Edit the script to include your specific S3 bucket name and the substring you are looking for.
   
   Example:
   ```python
   s3_bucket_name = 'your-bucket-name'
   substring = 'search-term'
   ```

2. Run the script.
   
   ```bash
   python s3_text_search.py
   ```

The script outputs the names of the text files containing the specified substring.

### Enhanced Usage with Argument Parsing

For a more dynamic approach, consider implementing argument parsing using Python's `argparse` module. This allows you to pass the bucket name and substring as command-line arguments, improving usability and flexibility. For example:

```python
import argparse

# Set up argument parsing
parser = argparse.ArgumentParser(description="Search text files in S3 for a substring")
parser.add_argument("bucket", help="Name of the S3 bucket")
parser.add_argument("substring", help="Substring to search for")

args = parser.parse_args()

# Use args.bucket and args.substring in your script
```

## Note

- The script is limited to `.txt` files but can be adjusted for other file types.
- For large buckets, consider pagination in `list_objects_v2`.
- The script may not handle very large files efficiently. Consider streaming for such cases.

## Contributions

Feel free to fork, submit pull requests, suggest features, or report issues.

## References

https://boto3.amazonaws.com/v1/documentation/api/latest/guide/s3-example-configuring-buckets.html
https://stackoverflow.com/questions/45981950/how-to-specify-credentials-when-connecting-to-boto3-s3
https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html

https://chat.openai.com for Python syntaxes

---
